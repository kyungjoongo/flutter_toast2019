import 'dart:async';

import 'package:flutter/services.dart';

class FlutterToast2019 {
    static const MethodChannel _channel = MethodChannel('flutter_toast2019');
    static const String ToastAndroid_BOTTOM = 'bottom';
    static const String ToastAndroid_CENTER = 'center';
    static const String ToastAndroid_TOP = 'top';
    static const String ToastAndroid_SHORT = 'short';
    static const String ToastAndroid_LONG = 'long';


    static Future<String> showToast(message, [gravity='bottom', duration = "short"]) async {
        Map map = {
            "message": message,
            "gravity": gravity,
            "duration": duration,
        };
        /*print('duration===>'+duration );
        print('gravity===>'+gravity );*/

        await _channel.invokeMethod('showToast', map);
        return "Success";
    }
}
