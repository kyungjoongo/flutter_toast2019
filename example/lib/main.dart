import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_toast2019/flutter_toast2019.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
    @override
    _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
    String _platformVersion = 'Unknown';

    @override
    void initState() {
        super.initState();
        //initPlatformState();
    }

    /*// Platform messages are asynchronous, so we initialize in an async method.
    Future<void> initPlatformState() async {
        String platformVersion;
        // Platform messages may fail, so we use a try/catch PlatformException.
        try {
            platformVersion = await FlutterToast2019.showToast;
        } on PlatformException {
            platformVersion = 'Failed to get platform version.';
        }

        if (!mounted)
            return;

        setState(() {
            _platformVersion = platformVersion;
        });
    }*/

    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            home: Scaffold(
                appBar: AppBar(
                    title: const Text('Plugin example app'),
                ),
                body: Center(
                    child: Column(
                        children: <Widget>[
                            RaisedButton(
                                onPressed: () async {
                                    await FlutterToast2019.showToast('고경준 천재님이십ㄴelkslkdflksdf', 'top');
                                },
                                child: Text('top'),
                            ),
                            RaisedButton(
                                onPressed: () async {
                                    await FlutterToast2019.showToast('고경준 천재님이십ㄴelkslkdflksdf', 'center', 'long');
                                },
                                child: Text('center___long'),
                            ),
                            RaisedButton(
                                onPressed: () async {
                                    await FlutterToast2019.showToast('고경준 천재님이십ㄴelkslkdflksdf', 'center');
                                },
                                child: Text('center___short'),
                            ),
                            RaisedButton(
                                onPressed: () async {
                                    await FlutterToast2019.showToast('고경준 234234234', 'bottom');
                                },
                                child: Text('bottom'),
                            ),


                        ],
                    ),
                ),
            ),
        );
    }
}
