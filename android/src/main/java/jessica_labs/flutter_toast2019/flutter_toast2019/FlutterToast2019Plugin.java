package jessica_labs.flutter_toast2019.flutter_toast2019;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.plugin.platform.PlatformViewRegistry;
import io.flutter.view.FlutterView;
import io.flutter.view.TextureRegistry;

/**
 * FlutterToast2019Plugin
 */
public class FlutterToast2019Plugin implements MethodCallHandler {

    Registrar registrar;

    public FlutterToast2019Plugin(Registrar registrar) {

        this.registrar = registrar;
    }

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_toast2019");
        channel.setMethodCallHandler(new FlutterToast2019Plugin(registrar));
    }


    @Override
    public void onMethodCall(MethodCall call, Result result) {

        if (call.method.equals("showToast")) {

            String message = call.argument("message");
            String gravity = call.argument("gravity");
            String duration = call.argument("duration");
            /*Log.d("Sdf", "onMethodCall: " + duration.toString());
            Log.d("gravity", "gravity===>: " + gravity.toString());*/

            Toast mytoast = Toast.makeText(registrar.context(), message, duration.toString().equals("short") ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG);


            if (gravity.toString().equals("center")) {
                mytoast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);       // for center vertical
            } else if (gravity.toString().equals("top")) {
                mytoast.setGravity(Gravity.TOP, 0, 0);       // for center vertical
            } else {
                mytoast.setGravity(Gravity.BOTTOM, 0, 0);       // for ce
            }

            mytoast.show();
        } else {
            result.notImplemented();
        }
    }
}
